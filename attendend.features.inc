<?php
/**
 * @file
 * attendend.features.inc
 */

/**
 * Implements hook_views_api().
 */
function attendend_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function attendend_node_info() {
  $items = array(
    'attendences' => array(
      'name' => t('Attendences'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'employee' => array(
      'name' => t('Employee'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
