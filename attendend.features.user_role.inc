<?php
/**
 * @file
 * attendend.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function attendend_user_default_roles() {
  $roles = array();

  // Exported role: employee.
  $roles['employee'] = array(
    'name' => 'employee',
    'weight' => 3,
  );

  return $roles;
}
